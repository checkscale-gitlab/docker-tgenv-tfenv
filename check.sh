#!/bin/bash

apk add curl jq

[[ ! -f EXISTING_TGENV ]] || touch EXISTING_TGENV
EXISTING_TGENV=$(cat EXISTING_TGENV)
echo "Existing tgenv: ${EXISTING_TGENV}"

if [[ -n $OVERWRITE_TGENV ]]; then
  echo "Overwriting tgenv: $OVERWRITE_TGENV"
  LATEST_TGENV=$OVERWRITE_TGENV
else
  LATEST_TGENV=$(curl -ks https://api.github.com/repos/cunymatthieu/tgenv/git/refs/tags | jq -r '.[-1].ref' | cut -d'/' -f3)
  echo "Latest tgenv: ${LATEST_TGENV}"
fi

[[ ! -f EXISTING_TFENV ]] || touch EXISTING_TFENV
EXISTING_TFENV=$(cat EXISTING_TFENV)
echo "Existing tfenv: ${EXISTING_TFENV}"

if [[ -n $OVERWRITE_TFENV ]]; then
  echo "Overwriting tfenv: $OVERWRITE_TFENV"
  LATEST_TFENV=$OVERWRITE_TFENV
else
  LATEST_TFENV=$(curl -ks https://api.github.com/repos/tfutils/tfenv/git/refs/tags | jq -r '.[-1].ref' | cut -d'/' -f3)
  echo "Latest tfenv: ${LATEST_TFENV}"
fi

if [[ (-n "${LATEST_TGENV}" && "${LATEST_TGENV}" != "${EXISTING_TGENV}") || (-n "${LATEST_TFENV}" && "${LATEST_TFENV}" != "${EXISTING_TFENV}") ]]; then
  echo "${LATEST_TGENV}" > LATEST_TGENV
  echo "${LATEST_TFENV}" > LATEST_TFENV
  echo "Building..."
fi
